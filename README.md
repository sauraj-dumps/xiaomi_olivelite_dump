## olivelite-user 10 QKQ1.191014.001 V12.5.1.0.QCPINXM release-keys
- Manufacturer: xiaomi
- Platform: msm8937
- Codename: olivelite
- Brand: Xiaomi
- Flavor: olivelite-user
- Release Version: 10
- Id: QKQ1.191014.001
- Incremental: V12.5.1.0.QCPINXM
- Tags: release-keys
- CPU Abilist: armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-GB
- Screen Density: 320
- Fingerprint: Xiaomi/olivelite/olivelite:10/QKQ1.191014.001/V12.5.1.0.QCPINXM:user/release-keys
- OTA version: 
- Branch: olivelite-user-10-QKQ1.191014.001-V12.5.1.0.QCPINXM-release-keys
- Repo: xiaomi_olivelite_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
